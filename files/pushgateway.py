import sys
import os
import re
from prometheus_client import CollectorRegistry, Gauge, push_to_gateway


def print_tempest_result(test_output):
    pattern = re.compile(r"[=]{2,}\n[\s\S]*", re.MULTILINE)
    print(pattern.search(test_output).string)


def extract_failed_tests(test_output):
    pattern = re.compile(
        r"^.*(?P<failed_test>tempest.*)[\)\[].*\n^[-]+$", re.MULTILINE
    )
    return pattern.findall(test_output)


def extract_test_stats(test_output):
    pattern = re.compile(
        r"^Ran: (?P<total>[0-9]+) tests in [0-9]+.[0-9]+ sec.\n"
        r" - Passed: (?P<passed>[0-9]+)\n"
        r" - Skipped: (?P<skipped>[0-9]+)\n"
        r" - Expected Fail: (?P<expected_fail>[0-9]+)\n"
        r" - Unexpected Success: (?P<unexptected_success>[0-9]+)\n"
        r" - Failed: (?P<failed>[0-9]+)\n",
        re.MULTILINE,
    )
    matches = pattern.search(test_output)
    return matches.groupdict()


def generate_stats_metrics(registry, stats):
    g = Gauge(
        "tempest_last_run",
        "Last time the tempest job terminated.",
        registry=registry,
    )
    g.set_to_current_time()
    Gauge(
        "tempest_tests_total_count", "number of all tests", registry=registry
    ).set(int(stats["total"]))
    Gauge(
        "tempest_tests_passed_count",
        "number of passed tests",
        registry=registry,
    ).set(int(stats["passed"]))
    Gauge(
        "tempest_tests_skipped_count",
        "number of skipped tests",
        registry=registry,
    ).set(int(stats["skipped"]))
    Gauge(
        "tempest_tests_exptected_fail_count",
        "number of expected failed tests",
        registry=registry,
    ).set(int(stats["expected_fail"]))
    Gauge(
        "tempest_tests_unexpected_success_count",
        "number of unexptected successful tests",
        registry=registry,
    ).set(int(stats["unexptected_success"]))
    Gauge(
        "tempest_tests_failed_count",
        "number of failed tests",
        registry=registry,
    ).set(int(stats["failed"]))


def generate_failed_tests_metrics(registry, failed):
    g = Gauge(
        "tempest_failed_test",
        "Failed Tempest test",
        ["test_name"],
        registry=registry,
    )
    for test in failed:
        g.labels(test).set(1)


def main() -> int:
    with open(sys.argv[1], "r") as file1:
        test_output = file1.read()
    print_tempest_result(test_output)
    print("Parsing Tempest Output")
    stats = extract_test_stats(test_output)
    failed = extract_failed_tests(test_output)
    registry = CollectorRegistry()
    generate_stats_metrics(registry, stats)
    generate_failed_tests_metrics(registry, failed)
    push_to_gateway(
        os.getenv("PUSHGATEWAY_URL"),
        os.getenv("PUSHGATEWAY_JOBNAME"),
        registry=registry,
    )
    print("Metrics pushed to Pushgateway")


if __name__ == "__main__":
    sys.exit(main())
