#!/bin/bash
set -euo pipefail
tempest_exit_code=0
REMOVE_PROJECTS="false"
ACCOUNT_CLEANUP="${ACCOUNT_CLEANUP:=False}"
accounts_file="/home/tempest/accounts.yaml"
tempest_config="/home/tempest/etc/tempest.conf"


trap exit_trap exit

function exit_trap() {
    if [[ "${ACCOUNT_CLEANUP}" == "True" && -f $accounts_file ]]; then
        # if it fails here the exit code wont be from tempest.
        account_cleanup
    fi
    exit $((tempest_exit_code))
}

function fail_if_equal() {
    if [[ "$1" == "$2" ]]; then
        echo "compare failed for: $3"
        return 1;
    fi
}

function account_cleanup() {
    echo -e "\nStarting cleanup with ospurge..."
    yq -c .[] < $accounts_file | while read -r project; do
        echo -ne "\nRunning Cleanup for Project Name:"; jq -r .project_name <<< "$project";
        project_id=$(jq -r .project_id <<< "$project")
        if [[ -z "${project_id}" ]]; then
            echo "project_id is missing in accounts.yaml. Cannot purge project!"
            continue
        fi
        username=$(jq -r .username <<< "$project")
        password=$(jq -r .password <<< "$project")
        fail_if_equal "$username" "$OS_USERNAME" "user $username" || continue
        fail_if_equal "$password" "$OS_PASSWORD" "password of $username" || continue
        fail_if_equal "$project_id" "${OS_PROJECT_ID:=}" "project_id $project_id" || continue

        env -u OS_USERNAME -u OS_PROJECT_NAME -u OS_PASSWORD -u OS_PROJECT_ID \
             /usr/local/bin/ospurge \
            --purge-own-project \
            --os-auth-url "${OS_AUTH_URL}" \
            --os-project-id "${project_id}" \
            --os-password "${password}" \
            --os-username "${username}" \
            --os-domain-name "${OS_DOMAIN_NAME:=default}" \
            --os-region-name "${OS_REGION_NAME}" \
            --verbose
        if [[ "${REMOVE_PROJECTS}" == "true" ]]; then
            echo "Deleting Project ${project_id} and user ${username}."
            openstack user delete "${username}"
            openstack project delete "${project_id}"
        fi
    done
}

# create accounts.yaml if not already provided
if [[ "${ACCOUNT_CLEANUP}" == "True" && ! -f $accounts_file ]]; then
    tempest account-generator --with-admin -c $tempest_config $accounts_file || \
        { echo "account-generator failed"; exit 1; }
    REMOVE_PROJECTS="true"
fi
if [[ -z "${PUSHGATEWAY_URL:=}" ]]; then
    tempest "$@" || tempest_exit_code=$?
else
    tempest "$@" > tempest.out || tempest_exit_code=$?
    python3 /pushgateway.py tempest.out
fi
