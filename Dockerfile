##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
FROM python:3.8-slim-bullseye@sha256:e191a71397fd61fbddb6712cd43ef9a2c17df0b5e7ba67607128554cd6bff267

ARG tempest_version
ARG barbican_tempest_plugin_version
ARG designate_tempest_plugin_version
ARG telemetry_tempest_plugin_version
ARG openstack_release

ARG tempest_uid=2500014
ARG tempest_gid=2500014

COPY files/ /
COPY build_files/ /build_files/

RUN set -eux ; \
    export LANG=C.UTF-8 ; \
    BUILD_PACKAGES="git patch build-essential wget" ; \
    RUN_PACKAGES="jq iputils-ping" ; \
    #
    apt-get update ; \
    apt-get install -y --no-install-recommends ${RUN_PACKAGES} ${BUILD_PACKAGES} ; \
    #
    ( git clone https://opendev.org/openstack/requirements.git --depth 1 --branch stable/${openstack_release} \
      || git clone https://opendev.org/openstack/requirements.git --depth 1 --branch ${openstack_release}-eol \
      || git clone https://opendev.org/openstack/requirements.git --depth 1 --branch unmaintained/${openstack_release} ) ; \
    git clone https://opendev.org/openstack/tempest.git --depth 1 --branch ${tempest_version} ; \
    git clone https://opendev.org/openstack/barbican-tempest-plugin.git --depth 1 --branch ${barbican_tempest_plugin_version} ; \
    git clone https://opendev.org/openstack/designate-tempest-plugin.git --depth 1 --branch ${designate_tempest_plugin_version} ; \
    git clone https://opendev.org/openstack/telemetry-tempest-plugin.git --depth 1 --branch ${telemetry_tempest_plugin_version} ; \
    # apply upper-constraints file to requirements only, because tempest is part of that file and will conflict with itself
    pip3 install -c requirements/upper-constraints.txt tempest/ ; \
    pip3 install -r /build_files/requirements.txt; \
    pip3 install -c requirements/upper-constraints.txt barbican-tempest-plugin/ ; \
    pip3 install -c requirements/upper-constraints.txt designate-tempest-plugin/ ; \
    pip3 install -c requirements/upper-constraints.txt telemetry-tempest-plugin/ ; \
    patch -d /usr/local/lib/python3.8/site-packages/tempest -p 2 -i /build_files/password_strength.patch ; \
    patch -d /usr/local/lib/python3.8/site-packages/tempest -p 2 -i /build_files/tempest-accounts-project-id.patch ; \
    #
    groupadd --gid ${tempest_gid} tempest ; \
    adduser --system --uid ${tempest_uid} --gid ${tempest_gid} tempest ; \
    mkdir -p /home/tempest/logs /home/tempest/etc ; \
    chown -R tempest:tempest /home/tempest/ ; \
    #
    # add a cirros image file for certain scenario tests https://gitlab.com/yaook/images/tempest/-/issues/3
    wget https://download.cirros-cloud.net/0.6.2/cirros-0.6.2-x86_64-disk.img -O /home/tempest/cirros-0.6.2-x86_64-disk.img; \
    chown -R tempest:tempest /home/tempest/cirros-0.6.2-x86_64-disk.img; \
    #
    rm -rf /build_files ; \
    rm -rf requirements tempest ; \
    rm -rf /var/lib/apt/lists/* ; \
    apt-get purge --auto-remove -y ${BUILD_PACKAGES} ; \
    rm -rf /root/.cache

WORKDIR /home/tempest
USER tempest

CMD ["/run_tempest.sh"]
